FROM openjdk:8-jre-alpine
VOLUME /tmp
ENV SPRING_PROFILES_ACTIVE docker

ADD build/libs/web-interface-0.0.1-SNAPSHOT.jar app.jar

RUN /bin/sh -c 'touch app.jar'

EXPOSE 8080

ENV app_name "gateway API"

ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar
