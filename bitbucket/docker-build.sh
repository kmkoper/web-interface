#!/bin/sh
#
docker login repo.treescale.com -u $DOCKER_LOGIN -p $DOCKER_PASS
if [[ $BITBUCKET_BRANCH = master ]]
then
    docker build -t repo.treescale.com/elephantcode/web-interface:$BITBUCKET_BUILD_NUMBER .
    docker push repo.treescale.com/elephantcode/web-interface:$BITBUCKET_BUILD_NUMBER
    docker tag repo.treescale.com/elephantcode/web-interface:$BITBUCKET_BUILD_NUMBER repo.treescale.com/elephantcode/web-interface:latest
    docker push repo.treescale.com/elephantcode/web-interface:latest
else
    docker build -t repo.treescale.com/elephantcode/web-interface:FEATURE-$BITBUCKET_BUILD_NUMBER .
    docker push repo.treescale.com/elephantcode/web-interface:FEATURE-$BITBUCKET_BUILD_NUMBER
fi