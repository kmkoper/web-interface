package pl.elephantcode.webinterface;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import pl.elephantcode.webinterface.user.UserResource;
import pl.elephantcode.webinterface.user.model.User;

@Controller
@AllArgsConstructor
@Slf4j
public class WebController {
    private final UserResource userResource;

    @GetMapping({"/", "", "/index", "/index.html"})
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String admin(Model model) {
        return "admin";
    }

    @PostMapping({"/signup", ""})
    public ModelAndView registerUserAccount(
            @ModelAttribute("user") User account,
            BindingResult result,
            WebRequest request,
            Errors errors) {
        User registered = new User();
        if (!result.hasErrors()) {
            registered = createUserAccount(account, result);
        }
        if (registered == null) {
            result.rejectValue("email", "message.regError");
        }
        if (result.hasErrors()) {
            return new ModelAndView("registration", "user", account);
        } else {
            return new ModelAndView("index", "user", account);
        }
    }

    private User createUserAccount(User account, BindingResult result) {
        return userResource.registerNewUser(account);
    }
}
