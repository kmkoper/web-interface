package pl.elephantcode.webinterface;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@Slf4j
@EnableEurekaClient
@SpringBootApplication
@EnableCaching
public class WebInterfaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebInterfaceApplication.class, args);
    }

}
