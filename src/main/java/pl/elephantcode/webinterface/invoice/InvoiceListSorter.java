package pl.elephantcode.webinterface.invoice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.elephantcode.webinterface.invoice.model.InvoiceDTO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
@Slf4j
public class InvoiceListSorter {

    public List<InvoiceDTO> sortAll(List<InvoiceDTO> list) {
        List<InvoiceDTO> overdueAndUnpaid = getOverdueAndUnpaid(list);
        list.removeAll(overdueAndUnpaid);

        List<InvoiceDTO> paid = getPaid(list);
        list.removeAll(list.stream().filter(isPaid()).collect(Collectors.toList()));


        List<InvoiceDTO> recentAndUnpaid = getOncomingAndUnpaid(list);
        list.removeAll(recentAndUnpaid);

        List<InvoiceDTO> sortedList = new ArrayList<>();
        sortedList.addAll(overdueAndUnpaid);
        sortedList.addAll(recentAndUnpaid);
        sortedList.addAll(paid);
        return sortedList;
    }

    public List<InvoiceDTO> sortAllByDate(List<InvoiceDTO> list) {
        return list.stream().sorted(sortDescendingByDate).collect(Collectors.toList());
    }

    public List<InvoiceDTO> getPaid(List<InvoiceDTO> list) {
        return list.stream().filter(isPaid()).sorted(sortDescendingByDate).collect(Collectors.toList());
    }

    public List<InvoiceDTO> getOncomingAndUnpaid(List<InvoiceDTO> list) {
        return list.stream().filter(isOncomingAndUnpaid()).sorted(sortAscendingByDate).collect(Collectors.toList());
    }

    public List<InvoiceDTO> getOverdueAndUnpaid(List<InvoiceDTO> list) {
        return list.stream().filter(isOverdueAndUnpaid()).sorted(sortAscendingByDate).collect(Collectors.toList());
    }

    private Comparator<InvoiceDTO> sortAscendingByDate =
            Comparator.comparing(t -> t.getPaymentDate().toLocalDate());

    private Comparator<InvoiceDTO> sortDescendingByDate =
            (t1, t2) -> t2.getPaymentDate().toLocalDate().compareTo(t1.getPaymentDate().toLocalDate());

    private Predicate<InvoiceDTO> isOverdueAndUnpaid() {
        return p -> p.getPaymentDate().isBefore(LocalDateTime.now()) && !p.isCurrentPaid();
    }

    private Predicate<InvoiceDTO> isPaid() {
        return InvoiceDTO::isCurrentPaid;
    }

    private Predicate<InvoiceDTO> isOncomingAndUnpaid() {
        return p -> p.getPaymentDate().isAfter(LocalDateTime.now()) && !p.isCurrentPaid();
    }

}

