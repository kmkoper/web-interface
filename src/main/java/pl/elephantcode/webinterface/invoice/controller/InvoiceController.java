package pl.elephantcode.webinterface.invoice.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.elephantcode.webinterface.invoice.InvoiceListSorter;
import pl.elephantcode.webinterface.invoice.InvoiceResource;
import pl.elephantcode.webinterface.invoice.model.InvoicesDTOList;
import pl.elephantcode.webinterface.user.UserResource;

import java.security.Principal;

@Controller
@AllArgsConstructor
@Slf4j
public class InvoiceController {

    private final InvoiceResource invoiceResource;
    private final UserResource userResource;
    private final InvoiceListSorter invoiceListSorter;


    @GetMapping("/invoices")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public String loadInvoices() {
        return "invoices";
    }

    @GetMapping("/invoices/ajax/load-table/{tableName}")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public String loadTable(@PathVariable("tableName") String tableName, Principal principal, Model model) {
        InvoicesDTOList invoices = invoiceResource.fetchAllInvoices(retrieveUserId(principal));

        switch (tableName) {
            case "allInvoices":
                model.addAttribute("allInvoices", invoiceListSorter.sortAllByDate(invoices.getInvoiceDTOList()));
                break;
            case "overdueAndUnpaid":
                model.addAttribute("overdueAndUnpaid", invoiceListSorter.getOverdueAndUnpaid(invoices.getInvoiceDTOList()));
                break;
            case "oncomingAndUnpaid":
                model.addAttribute("oncomingAndUnpaid", invoiceListSorter.getOncomingAndUnpaid(invoices.getInvoiceDTOList()));
                break;
            case "paid":
                model.addAttribute("paid", invoiceListSorter.getPaid(invoices.getInvoiceDTOList()));
                break;
            default:
                log.error("No such table {} found", tableName);


        }
        return "fragments/invoices-lists :: " + tableName;
    }

    @GetMapping("/invoices/ajax/load-invoice/{invoiceId}")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public String loadInvoice(@PathVariable("invoiceId") Integer invoiceId, Principal principal, Model model) {

        //fetching all invoices because they are already cached at this moment
        InvoicesDTOList invoices = invoiceResource.fetchAllInvoices(retrieveUserId(principal));

        invoices.getInvoiceDTOList().forEach(it -> {
            if (it.getId().equals(invoiceId)) {
                model.addAttribute("invoice", it);
            }
        });
        return "fragments/edit-box :: modal-replacement";
    }

    @GetMapping("/invoices/ajax/add-new")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public String addNewInvoice() {

        return "fragments/add-new-box :: modal-replacement";
    }

    private Integer retrieveUserId(Principal principal) {
        return userResource.getUserByMail(principal.getName()).getId();
    }
}
