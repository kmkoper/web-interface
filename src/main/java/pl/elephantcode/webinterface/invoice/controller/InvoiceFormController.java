package pl.elephantcode.webinterface.invoice.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.elephantcode.webinterface.invoice.InvoiceResource;
import pl.elephantcode.webinterface.invoice.model.InvoiceDTO;
import pl.elephantcode.webinterface.user.UserResource;

import java.security.Principal;

@Slf4j
@Controller
@AllArgsConstructor
public class InvoiceFormController {

    private final InvoiceResource invoiceResource;
    private final UserResource userResource;

    @PostMapping("/invoices")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void updateInvoice(@ModelAttribute InvoiceDTO invoice, Principal principal) {
        if (retrieveUserId(principal).equals(invoice.getUserId())) {
            invoiceResource.updateInvoice(invoice);
        } else {
            log.error("Someone tried to edit invoice of different user!");
        }
    }

    @PostMapping("/invoices/add")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public void addNewInvoice(@ModelAttribute InvoiceDTO invoice, Principal principal) {
        invoice.setUserId(retrieveUserId(principal));
        invoiceResource.addNewInvoice(invoice);
    }

    private Integer retrieveUserId(Principal principal) {
        return userResource.getUserByMail(principal.getName()).getId();
    }


}
