package pl.elephantcode.webinterface.invoice.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode
public class InvoiceDTO {

    private Integer id;
    private String description;
    private Float amount;
    private Integer userId;
    private boolean isActive;
    private boolean isRecurring;
    private Integer paymentPeriod;
    private String paymentPeriodType;
    private boolean isCurrentPaid;
    private LocalDateTime paymentDate;
    private Integer reminderBeforePaymentValue;
    private String reminderBeforePaymentType;
    private LocalDate paymentDateOnly;

    //ugly workaround to show only date and parse it from string
    public void setPaymentDateOnly(String dateString) {
        if (!dateString.isEmpty()) {
            LocalDate date = LocalDate.parse(dateString);
            this.paymentDate = LocalDateTime.of(date.getYear(), date.getMonth(), date.getDayOfMonth(), 01, 00, 00);
            this.paymentDateOnly = date;
        } else
            this.paymentDate = null;
    }

    public LocalDate getPaymentDateOnly() {
        return paymentDate.toLocalDate();
    }

}
