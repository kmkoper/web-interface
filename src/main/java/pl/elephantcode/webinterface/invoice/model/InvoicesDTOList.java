package pl.elephantcode.webinterface.invoice.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode
public class InvoicesDTOList {
    private List<InvoiceDTO> invoiceDTOList;

    public InvoicesDTOList() {
        invoiceDTOList = new ArrayList<>();
    }
}
