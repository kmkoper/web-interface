package pl.elephantcode.webinterface.security;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.elephantcode.webinterface.user.UserResource;
import pl.elephantcode.webinterface.user.model.User;

@Service
@Slf4j
@AllArgsConstructor
public class UserService implements UserDetailsService {
    private final UserResource userResource;

    @Override
    public UserDetails loadUserByUsername(final String userMail) throws UsernameNotFoundException {
        final User user = userResource.getUserByMail(userMail);
        if (user == null) {
            throw new UsernameNotFoundException("User with mail: {" + userMail + "} not found");
        }
        return new UserPrincipal(user);
    }

}
