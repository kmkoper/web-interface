package pl.elephantcode.webinterface.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.elephantcode.webinterface.user.model.User;


@Component
@Slf4j
public class UserResource {

    @LoadBalanced
    @Bean
    RestTemplate restTemplateUser() {
        return new RestTemplate();
    }

    @Autowired
    private RestTemplate restTemplateUser;

    @Cacheable(value = "user", key = "#userMail")
    public User getUserByMail(final String userMail) {
        return restTemplateUser.getForObject("http://gatewayApi/user-api/api/users/mail/" + userMail, User.class);
    }

    public User registerNewUser(User user) {
        if (!(user.getMail() == null) && !(user.getName() == null) && !(user.getPassword() == null)) {
            user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(11)));
            return restTemplateUser.postForObject("http://gatewayApi/user-api/api/users/", user, User.class);
        } else {
            throw new IllegalArgumentException("User mail, name and password cannot be empty!");
        }
    }

    public void confirmRegistration(String activationHash) {
        restTemplateUser.put("http://gatewayApi/user-api/api/users/activate/" + activationHash,
                null, User.class);
    }
}
