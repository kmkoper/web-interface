package pl.elephantcode.webinterface.user.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import pl.elephantcode.webinterface.user.UserResource;
import pl.elephantcode.webinterface.user.model.User;

import java.security.Principal;

@Controller
@AllArgsConstructor
@Slf4j
public class UserController {
    private final UserResource userResource;


    @GetMapping("/user")
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public String user(Model model, Principal principal) {
        User user = userResource.getUserByMail(principal.getName());
        model.addAttribute("user", user);
        return "user";
    }

    @GetMapping("/user/ajax/signup-form")
    public String signUp(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "fragments/signup :: modal-replacement";
    }

    @GetMapping("/user/activate/{activationHash}")
    public ModelAndView activateUser(@PathVariable("activationHash") String activationHash, ModelMap model) {
        userResource.confirmRegistration(activationHash);
        return new ModelAndView("redirect:/login", model);
    }

}
