package pl.elephantcode.webinterface.user.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;

@Data
@EqualsAndHashCode
@ToString
public class Role implements GrantedAuthority {


    private Integer id;
    private String role;

    @Override
    public String getAuthority() {
        return this.role;
    }
}
