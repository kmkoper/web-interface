package pl.elephantcode.webinterface.user.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@EqualsAndHashCode
@ToString
public class User {

    private Integer id;
    private String name;
    private String mail;
    private String password;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private boolean isActive;
    private Set<Role> roles;

}
