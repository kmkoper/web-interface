$(document).ready(function () {
    $(".menu").click(function () {
        var url = "/invoices/ajax/load-table/" + this.id + " .content";
        $(".content-container").replaceWith($('<div class="content-container">').load(url));
    });
});

$(document).on("click", '.row', function (event) {
    var url = "/invoices/ajax/load-invoice/" + this.id;
    $(".modal-replacement").replaceWith($('<div class="modal-replacement">').load(url));
    $(".modal").css('display', 'inline-block');
});

$(document).on("click", '.add-new-btn', function (event) {
    var url = "/invoices/ajax/add-new" + this.id;
    $(".modal-replacement").replaceWith($('<div class="modal-replacement">').load(url));
    $(".modal").css('display', 'inline-block');
});

$(document).on("click", '.signup-btn', function (event) {
    var url = "/user/ajax/signup-form";
    $(".modal-replacement").replaceWith($('<div class="modal-replacement">').load(url));
    $(".modal").css('display', 'inline-block');
});

$(document).on("click", '.close', function (event) {
    $(".modal-replacement").replaceWith($('<div class="modal-replacement"></div>'));
    $(".modal").css('display', 'none')
});


$('input:radio[name="recurring"]').change(function () {
    if (this.checked) {
        if (this.value == 'true') {
            $('#payment-period').prop('required', 'required');
            $('#payment-period-type').prop('required', 'required');
            $('#payment-period-type-selector').prop('disabled', 'disabled');
        } else {
            $('#payment-period').removeAttr('required');
            $('#payment-period-type').removeAttr('required');
            $('#payment-period-type-selector').removeAttr('disabled');
        }
    }
});

$('input:radio[name="active"]').change(function () {
    if (this.checked) {
        if (this.value == 'true') {
            $('#reminder-before-payment-value').prop('required', 'required');
            $('#reminder-before-payment-type').prop('required', 'required');
            $('#reminder-before-payment-type-selector').prop('disabled', 'disabled');
        } else {
            $('#reminder-before-payment-value').removeAttr('required');
            $('#reminder-before-payment-type').removeAttr('required');
            $('#reminder-before-payment-type-selector').removeAttr('disabled');

        }
    }
});