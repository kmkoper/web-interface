package pl.elephantcode.webinterface.invoice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.elephantcode.webinterface.invoice.model.InvoiceDTO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class InvoiceListSorterTest {
    InvoiceListSorter sorter;
    List<InvoiceDTO> unsortedList;

    InvoiceDTO overdueAndUnpaid;
    InvoiceDTO overdueAndPaid;
    InvoiceDTO recentAndUnpaid;
    InvoiceDTO inFutureAndUnpaid;
    InvoiceDTO recentAndPaid;
    InvoiceDTO longOverdueAndUnpaid;

    @BeforeEach
    void setUp() {
        sorter = new InvoiceListSorter();
        unsortedList = new ArrayList<>();

        overdueAndUnpaid = new InvoiceDTO();
        overdueAndUnpaid.setId(0);
        overdueAndUnpaid.setCurrentPaid(false);
        overdueAndUnpaid.setPaymentDate(LocalDateTime.now().minusDays(1L));

        longOverdueAndUnpaid = new InvoiceDTO();
        longOverdueAndUnpaid.setId(5);
        longOverdueAndUnpaid.setCurrentPaid(false);
        longOverdueAndUnpaid.setPaymentDate(LocalDateTime.now().minusDays(55L));

        overdueAndPaid = new InvoiceDTO();
        overdueAndPaid.setId(3);
        overdueAndPaid.setCurrentPaid(true);
        overdueAndPaid.setPaymentDate(LocalDateTime.now().minusDays(1L));

        recentAndUnpaid = new InvoiceDTO();
        recentAndUnpaid.setId(1);
        recentAndUnpaid.setCurrentPaid(false);
        recentAndUnpaid.setPaymentDate(LocalDateTime.now().plusDays(1L));

        recentAndPaid = new InvoiceDTO();
        recentAndPaid.setId(2);
        recentAndPaid.setCurrentPaid(true);
        recentAndPaid.setPaymentDate(LocalDateTime.now().plusDays(1L));

        inFutureAndUnpaid = new InvoiceDTO();
        inFutureAndUnpaid.setId(4);
        inFutureAndUnpaid.setCurrentPaid(false);
        inFutureAndUnpaid.setPaymentDate(LocalDateTime.now().plusDays(10L));

        unsortedList.add(recentAndPaid);
        unsortedList.add(recentAndUnpaid);
        unsortedList.add(overdueAndPaid);
        unsortedList.add(overdueAndUnpaid);

    }

    @Test
    void sortAll() {
        //given
        //setUp()

        //when
        List<InvoiceDTO> sortedList = sorter.sortAll(unsortedList);

        //then
        //1. unpaid and overdue, 2. unpaid and recent, 3. paid and the most recent, 4. paid and overdue
        assertEquals(4, sortedList.size());
        assertEquals(overdueAndUnpaid, sortedList.get(0));
        assertEquals(recentAndUnpaid, sortedList.get(1));
        assertEquals(recentAndPaid, sortedList.get(2));
        assertEquals(overdueAndPaid, sortedList.get(3));
    }

    @Test
    void getPaidTest() {
        //when
        List<InvoiceDTO> paidList = sorter.getPaid(unsortedList);

        //then
        //1.paid and the most recent one, 2. paid and overdue
        assertEquals(2, paidList.size());
        assertEquals(recentAndPaid, paidList.get(0));
        assertEquals(overdueAndPaid, paidList.get(1));
        assertFalse(paidList.contains(overdueAndUnpaid));
        assertFalse(paidList.contains(recentAndUnpaid));
    }

    @Test
    void getOncomingAndUnpaidTest() {
        //given
        unsortedList.add(inFutureAndUnpaid);

        //when
        List<InvoiceDTO> oncomingAndUnpaidList = sorter.getOncomingAndUnpaid(unsortedList);


        //then
        //1. the most recent one and unpaid
        assertEquals(2, oncomingAndUnpaidList.size());
        assertEquals(recentAndUnpaid, oncomingAndUnpaidList.get(0));
        assertEquals(inFutureAndUnpaid, oncomingAndUnpaidList.get(1));
        assertFalse(oncomingAndUnpaidList.contains(overdueAndUnpaid));
        assertFalse(oncomingAndUnpaidList.contains(overdueAndPaid));
        assertFalse(oncomingAndUnpaidList.contains(recentAndPaid));
    }


    @Test
    void getOverdueAndUnpaidTest() {
        //given
        unsortedList.add(longOverdueAndUnpaid);

        //when
        List<InvoiceDTO> overdueAndUnpaidList = sorter.getOverdueAndUnpaid(unsortedList);

        //then
        //1. long overdue and unpaid, 2. small overdue and unpaid
        assertEquals(2, overdueAndUnpaidList.size());
        assertEquals(longOverdueAndUnpaid, overdueAndUnpaidList.get(0));
        assertEquals(overdueAndUnpaid, overdueAndUnpaidList.get(1));
        assertFalse(overdueAndUnpaidList.contains(recentAndPaid));
        assertFalse(overdueAndUnpaidList.contains(overdueAndPaid));

    }
}