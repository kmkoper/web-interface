package pl.elephantcode.webinterface.security;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.elephantcode.webinterface.user.UserResource;
import pl.elephantcode.webinterface.user.model.Role;
import pl.elephantcode.webinterface.user.model.User;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {

    final String USER_MAIL = "test@test.pl";
    final String ROLE = "ROLE_ADMIN";
    final String PASSWORD = "PASS";
    final boolean IS_ACTIVE = true;

    User user;
    Role role;
    Set<Role> roleSet;

    @Mock
    UserResource userResource;

    @InjectMocks
    UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        roleSet = new HashSet<>();

        role = new Role();
        role.setId(1);
        role.setRole(ROLE);
        roleSet.add(role);

        user = new User();
        user.setMail(USER_MAIL);
        user.setRoles(roleSet);
        user.setPassword(PASSWORD);
        user.setActive(IS_ACTIVE);

    }


    @Test
    void loadUserByUsernameWithHappyPath() {
        Mockito.when(userResource.getUserByMail(USER_MAIL)).thenReturn(user);

        UserDetails userPrincipal = userService.loadUserByUsername(USER_MAIL);

        assertNotNull(userPrincipal);
        assertEquals(userPrincipal.getUsername(), USER_MAIL);
        assertTrue(userPrincipal.getAuthorities().contains(role));
        assertTrue(userPrincipal.isEnabled());
        assertEquals(userPrincipal.getPassword(), PASSWORD);

    }

    @Test
    void loadUserByUsernameWhenUserNotFoundShouldThrowException() {
        Mockito.when(userResource.getUserByMail(USER_MAIL)).thenReturn(null);

        assertThrows(UsernameNotFoundException.class, () ->
                userService.loadUserByUsername(USER_MAIL)
        );
    }

    @Test
    void loadUserByUsernameWhenUserIsNullShouldThrowException() {
        user = null;

        Mockito.when(userResource.getUserByMail(USER_MAIL)).thenReturn(user);

        assertThrows(UsernameNotFoundException.class, () ->
                userService.loadUserByUsername(USER_MAIL)
        );
    }
}