package pl.elephantcode.webinterface.user;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.client.RestTemplate;
import pl.elephantcode.webinterface.user.model.User;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class UserResourceTest {

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    UserResource userResource;

    User user;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        user = new User();
        user.setPassword("Test");
        user.setMail("test@test.test");
        user.setName("Test");
    }

    @Test
    void registerNewUserWhenFieldsAreFilled() {

        //when
        Mockito.when(restTemplate.postForObject(anyString(), any(User.class), any())).thenReturn(user);
        userResource.registerNewUser(user);

        //then
        verify(restTemplate, times(1)).postForObject(anyString(), any(User.class), any());
    }

    @Test
    void registerNewUserWhenNameMailAndPasswordAreNullShouldThrowException() {
        //given
        user = new User();

        //when
        Mockito.when(restTemplate.postForObject(anyString(), any(User.class), any())).thenReturn(user);
        assertThrows(IllegalArgumentException.class, () ->
                userResource.registerNewUser(user));

        //then
        verify(restTemplate, times(0)).postForObject(anyString(), any(User.class), any());
    }

    @Test
    void registerNewUserWhenNameAreEmpty() {
        //given
        user.setName(null);

        //when
        Mockito.when(restTemplate.postForObject(anyString(), any(User.class), any())).thenReturn(user);

        assertThrows(IllegalArgumentException.class, () ->
                userResource.registerNewUser(user));

        //then
        verify(restTemplate, times(0)).postForObject(anyString(), any(User.class), any());
    }
}